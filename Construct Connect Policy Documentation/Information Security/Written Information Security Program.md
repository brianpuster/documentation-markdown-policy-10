title: Written Information Security Program  
author: VerSprite vCISO  
published: 2020-06-03     


# Policy name:
Written Information Security Program

# Policy level:
Annual General

# Author:
VerSprite vCISO

# Approver(s)
Bob Ven CTO/CIO, Buck Brody CFO/EVP, Jeff Cryder EVP, Dana Oney SVP IT Ops  

# Location or Team Applicability
All locations, all teams

# Policy text:
**Preamble.** This Written Information Security Program provides an overview of administrative controls at ConstructConnect aligning to the NIST Cybersecurity Framework. This WISP is evolving. To date, the administrative controls targeted for development include the following:

- Access Control Policy
- Business Continuity and Disaster Recovery Policy
- Data Management Policy
- Incident Response Policy
- Risk Management Policy
- Vendor Risk Policy
- Business Continuity Plans
- Disaster Recovery Plans
- Incident Response Plan

## GENERAL

### Objective
This Written Information Security Program provides information on the measures used to establish the cyber security foundation controls and regulatory requirements. This plan is driven by many factors, with the key factor being risk, and sets the foundation under which Company operates and safeguards its data and systems to reasonably reduce risk and minimize the effect of potential incidents. Effective security is a team effort involving the participation and support of all users who interact with data and systems.

### Scope
The approach to this program and associated security policies is risk-based and focuses on data, systems and infrastructures used to store and/or process such data. Considerations for data classification, and risk associated with the data, should be made when implementing different levels of controls outlined in this guideline and associated policies to determine the scope of the systems, products and data impacted.
The guideline and associated policies do not supersede applicable law, or any existing labor management agreement that may be in effect as of the effective date of this guidance.

### Responsibilities
The following roles and responsibilities are to be developed and subsequently assigned to authorized personnel within Company regarding information Security Practices:

**Chief Information Officer (CIO)** is responsible for:
- providing overall direction, guidance, leadership and support for the entire information systems environment, while also assisting other applicable personnel in their day-to-day operations.
- The CIO is to report to other members of senior management on a regular basis regarding all aspects of the organization's information security posture.

**VP of IT Operations** is responsible for:
- guiding the implementing of baseline configuration standards for all in-scope system components. This requires obtaining a current and accurate asset inventory of all such systems, assessing their initial posture with the stated baseline, and undertaking the necessary configurations.
- monitoring compliance with the stated baseline configuration standards, reporting to senior management all instances of non-compliance and efforts undertaken to correct such issues. Additionally, due to the fact that these individuals are to undertake the majority of the operational and technical procedures for the organization, it is critical to highlight other relevant duties, such as the following:

**Corporate Information Security Office (CISO)** is responsible for:
- implementing, coordinating, and maintaining this WISP, including:
  - assessing internal and external risks to sensitive information and maintaining related documentation, including risk assessment reports and remediation plans;
  - coordinating the development, distribution and maintenance of information security policies and procedures; coordinating the design of reasonable and appropriate administrative, physical and technical safeguards to protect sensitive information;
  - ensuring that the safeguards are implemented and maintained to effectively protective sensitive information;
  - analyzing risk of service providers that access or maintain sensitive information on behalf of the Company;
  - monitoring and testing the information security program's implementation and effectiveness on an ongoing basis;
  - defining and managing incident response procedures.
- ensuring the asset inventory for all in-scope system components and network topology documents are in fact kept current and accurate;
- coordinating risk assessments; and
- providing periodic security updates on security posture, remediation roadmap plans and security incidents.

**Legal Department** is responsible for:
- reviewing, ensuring adequacy and retaining copies of all binding agreements, and
- advise regarding reported security incidents, evidence handling, breach notification and other related matters.

**Steering Committee** is comprised of:
- CIO,
- Senior leadership form various business units of the Company,
- CISO representativem, and
- Legal Department representative.

Steering Committing is responsible for:
- providing overall strategic leadership and direction for Information Technology through the alignment of IT and business strategic objectives; assisting in the development, review and evaluation of policies, plans and procedures; and promoting communication and collaboration between their respective business units, the IT Department and the CISO.

## Review
This policy must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CIO owns this Policy, but any update or change must be approved by the Information Security Committee (Steering Committee?).

### POLICIES

- **Access Control Policy.** This policy covers the main topics of Access Control: business requirements for Access Controls, User Access Management, User Responsibilities and System and Application Access Control.
- **Business Continuity and Disaster Recovery Policy.** This policy provides the basis for the continued provision of business-critical IT services and maintenance of information at an an acceptable level.
- **Data Management Policy.** This policy defines the Company's objectives for establishing specific standards for the classification, retention and disposal of information.
- ** This policy defines general terms, roles and corresponding responsibilities and requirements in the context of Information Security Incidents as well as a schema for categorizing incidents
- **Risk Management Policy.** This policy defines the Company's requirements for identification and reporting information security risks in an early stage to create transparency of the existing information security risks and to define mitigation measures for those risks.
- **Vendor Risk Policy.** This policies provides a framework for managing the vendor life-cycle, ensuring vendor integrity, and maintaining compliance for the Company and its vendors.

### PLANS & PROCEDURES

- **Business Continuity Plans.** These plans provide actionable guidance in the event of a disruption or unavailability of a particular business process or solution.
- **Disaster Recovery Plans.** These plans provide actionable guidance in the event of specific natural or man-made disaster.
- **Incident Response Plan.** This plan:
  - defines general terms, roles and corresponding responsibilities and requirements in the context of Information Security Incidents as well as a schema for categorizing incidents
  - provides actionable guidance on the detection, response and recovery to possible information security incidents.

### SAFEGUARDS
The Company must develop, implement and maintain reasonable administrative, technical and physical safeguards in accordance with applicable laws and standards to protect the security, confidentiality, integrity and availability of personal or other sensitive information that the Company owns or maintains on behalf of others.

Safeguards must:

- Be appropriate to the Company's size, scope and business; its available resources; and the amount of personal and other sensitive information that the Company owns or maintains on behalf of others, while recognizing the need to protect corporate, consumer and employee information;
- Be documented in the Company's information security policies and procedures;
- Contain administrative controls including, at a minimum;
  - Designating one or more employees to coordinate the information security program;
  - Identifying reasonably foreseeable internal and external risks, and assessing whether existing safeguards adequately control the identified risks;
  - Training employees in security program practices and procedures with management oversight;
  - Selecting service providers that can maintain appropriate safeguards, and requiring service providers to maintain safeguards by contract; and
  - Adjusting the information security program considering business changes or new circumstances.
- Contain physical controls including, at a minimum;
  - Defining and implementing reasonable physical security measures to protect areas where personal or other sensitive information may be accessed, including reasonably restricting physical access and storing records containing personal or other sensitive information in locked facilities, areas or containers;
  - Preventing, detecting and responding to intrusions or unauthorized access to personal or other sensitive information, including during or after data collection, transportation or disposal;
  - Securely destroying or disposing of personal or other sensitive information, whether in paper or electronic form, when it is no longer to be retained in accordance with applicable laws or accepted standards;
- Contain technical controls and maintenance including a security system covering the Company's networks and computers that supports, at a minimum;
  - Secure user authentication protocols, including;
  - Controlling user identification and authentication with a reasonably secure method of assigning and selecting passwords (ensuring that passwords are kept in a location or format that does not compromise security) or by using other technologies, such as biometrics, token devices or other multi-factor authentication;
  - Restricting access to active users and active user accounts only and preventing terminated employees or contractors from accessing systems or records; and
  - Blocking or placing limitations on user identifier's access after multiple unsuccessful attempts to gain access.
  - Secure access control and network security measures, including;
  - Restricting access to records and files containing personal or other sensitive information to those with a need to know to perform their duties; and
  - Assigning to each individual unique identifiers, passwords and authentication means that are reasonably designed to maintain security.
- Reasonable system monitoring for preventing, detecting and responding to unauthorized use of or access to personal or other sensitive information or other attacks or system failures;
- Reasonably current firewall protection and software patches for systems that contain, or may provide access to systems that contain, personal or other sensitive information;
- Encryption of all personal or other sensitive information traveling via wireless or across public networks;
- Encryption of all personal or other sensitive information stored on laptops or other portable or mobile devices, and to the extent technically feasible, personal or other sensitive information stored on any other device or media (data-at-rest);
- Reasonably current system security software, or a version that can still be supported with reasonably current patches and malicious software (“malware”) definitions, that;
- includes malware protection with reasonably current patches and malware definitions; and
- is configured to receive updates on a regular basis.

## REFERENCES

### Relevant Regulations
- PCI-DSS
- US data privacy laws

### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).
- Identify

### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).
- 1.1, 1.2, 1.3

### Related Policies, Plans, Procedures and Guidelines.
- Access Control Policy
- Data Management Policy
- Incident Response Policy
- Risk Management Policy
- Vendor Risk Policy

 
