title: Creating Policy
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement
published: 2019-07-31
effective date: 2019-09-01
policy level: Important Not Urgent
data classification: See [ConstructConnect Data Management Policy](https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Data%20Management%20Policy)
approvers:
    - Steve Testa - VP Strategic Initiatives and Continuous Improvement
    - Buck Brody - EVP Finance
location or team applicability: All
updated: 2020-09-03


# Policy Types and Communication Cadence

Very Important
: Communicated immediately by email to all team member the policy applies to.

Important Not Urgent
: Quarterly policy release by email from Program Management Team (PMO)

Annual General
: Acknowledged annually via electronic acknowledgement method

## Policy Data Classification

### Confidential

- Information that is extremely sensitive in nature and may not be shared except when deemed absolutely necessary for continuation of business. (E.g. usernames, passwords, 2FA/MFA, encryption keys, propriety business information, personal information of customers.)
- Confidential Data must be encrypted for internal and external communication
- Confidential Data must be encrypted at rest.
- Access to any Confidential Policy must only be granted when necessary and revoked as soon as it is no longer necessary.

### Sensitive

- Any non-public information about Company, its respective business operations, business plans, or IT systems. (E.g. Business Continuity or Disaster Recovery Plans, Procedures, Standards, Pricing Rules and Methods, Data Flow Diagrams.)
- Sensitive data must be encrypted for external communication.
- Sensitive data must be encrypted at rest.
- Access to any Sensitive Policy should be granted to all teams and locations to which that policy applies. Additional access must only be granted when necessary and revoked as soon as it is no longer necessary groups.

### Internal

- Information available to all employees. (E.g. General Company guidelines, training material, business contact information.)
- Internal Information must be encrypted for external communication.
- Policies classified as "Internal" may be shared with all Company employees.

# Policy Documentation Standard

- Policy documentation will be standardized by submission of the ConstructConnect Policy Documentation Template to the Program Management Team (PMO). Copy template below. 
- PMO will create the formal policy in a standard ConstructConnect format to be published centrally on the [ConstructConnect Documentation Service](https://policydocs.buildone.co/pages/policy).
- Policies may be located on The Loop or other team location with a ConstructConnect Documentation Service linking out to the existing policy location.

# Policy Approval

- Appropriate approval will be obtained through the PMO based on policy level and application.
- All team specific policies must get People & Culture approval prior to publishing or changing policies.
- All employment related policies must be approved by People & Culture. Plese refer to the [Employee Handbook](https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/2019%20ConstructConnect%20Employment%20Handbook)

# Policy Revisions

- Policy revisions should document the date of the change and state the nature of the change. All non-gramattical changes require approval from all policy approvers prior to being published and all revisions will be communicated per the level of importance of the policy.

# Policy Template

- Copy below text, complete and submit to the PMO for policy creations or updates.
- Do not use any formatting or bullets

```
# Policy name:
insert policy name

# Policy level:
Very Important, Important Not Urgent, Annual General

# Data Classification:
Public, Internal, Sensitive, Confidential

# Author:
Insert author name and title

# Approver(s):
Input approver name and title

# Location or Team Applicability:
What location(s) and/or team(s) does the policy apply to?

# Effective date:
input date using YYYY-MM-DD format

# Policy text:
insert policy text

# Insert any procedure or process documentation (optional):
insert text

# Revision History:
insert YYYY-MM-DD of change and nature of change
```

# Insert any procedure or process documentation (optional):

## Process for Creating or Updating Policies

- Verify that the editor has access to ConstructConnect / documentation-markdown-policy
- Verify that the editor and reviewers have access to the ConstructConnect's Office365 environment. 
- Within ConstructConnect's BitBucket, create a branch from ConstructConnect / documentation-markdown-policy. 
- Identify the leadership and SME that will review the proposed policy.
- If Confidential or Sensitive Policies
    - In OneDrive, populate the full proposed new or edited policy.
    - Within the BitBucket / ConstructConnect / documentation-markdown-policy branch, include the policy headers as normal, but replace the Policy Text with a hyperlink to the proposed policy in OneDrive and submit the commit.
    - Verify that BitBucket only contains the policy headers and the link to OneDrive.
- Within the branch, submit the commit with the proposed new or edited policy.
- Start the review process.
    - For reviewers with BitBucket access, populate the reviewers within BitBucket.
    - For any reviewer without BitBucket access:
    - Identify the reviewer in the policy header labeled “Reviewers”
    - Populate Steve Testa as a reviewer within Bitbucket
    - Steve will distribute the policy to the other reviewers via e-mail for comment.
- Reviewers review the proposal, recommend/make changes, and approve once they are satisfied with the version.
- Once all reviewers have approved, merge the ConstructConnect / documentation-markdown-policy branch into the Master ConstructConnect / documentation-markdown-policy.
- If Confidential or Sensitive Policies
    - Push the document with policy headers and the link to OneDrive on the policydocs.buildone.co page through BitBucket.
- Through BitBucket, publish on policydocs.buildone.co page.


