title: Sales, Renewals and Amendments Closed Won  
author: Beth Patrick, Manager Accounting Operations  
published: 2019-10-28  
policy level: Important Not Urgent  
approver(s): Jim Bangs, Sr. VP Sales Operations, Jim Hill, EVP & GM Trade Contractor, Howard Atkins, EVP & GM Building Products Manufacturers, Jon Kost, Executive VP & GM GC, Buck Brody, EVP Finance
applicable Locations: All Sales and C4L teams
effective date: 2019-10-28
last updated: 2019-12-17 Revised, 2020-07-08 Revised Controllership approval

---

# Policy Text

- A sale, whether new, renewal, or amendment is defined as securing payment and/or obtaining a signed agreement.
- Enterprise and On Center 12+ renewals will be accepted with persuasive language of agreement from the customer. That must be sent to the Team Manager and Accounts Receivable at <cincy-accountsreceivable@constructconnect.com>.
- The Closed Won date of the opportunity must match the date of payment or e-sign, whichever occurs first. 
- If the signed contract is obtained on the last day of the month, it will be counted within that month’s bookings. 
- Accounting will ensure the Closed Won date matches the payment and/or e-sign date. 
- Should you have questions surrounding the date, please reach out to the Sales Tools & Effectiveness Manager to review. 
- If there isn’t a payment or signed contract and the opportunity is closed/won, Accounting will change the opportunity back to prospecting and alert the sales rep.  

**All monthly sales require either ACH or credit card for auto pay. If we do not have those, the opportunity will be moved back to prospecting.**

**Per business rules, we will not accept partial payments for initial payments on sales or renewals. There will be no exceptions.** 


All future start dates, and future dated invoices must be within 30 days of the Closed Won opportunity.

- Within 30 days requires Sales/C4L Management approval
- Anything past 30 days requires Controller approval prior to signed agreement. The request should come from a Manager utilizing the [Payment Term Change or Delayed Start Form](https://forms.office.com/Pages/ResponsePage.aspx?id=NaAXdS_PBEKPsjsv72alF5WXlGMTGbVNoSI4tQNCwXBUMFBORVFKUFo1WlpWU0M5VjkzUVNHUTNHUy4u).
- Any net terms exceeding net 30 require Controller approval prior to signed agreement. See form above.
- Any net terms exceeding net 90 require EVP approval.

---