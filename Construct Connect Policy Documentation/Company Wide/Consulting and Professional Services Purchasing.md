title: Consulting and Professional Services Purchasing  
author: Dave Storer - Controller  
published: 2019-08-16  
policy level: Very Important  
approver(s): Bob Ven - CTO, Buck Brody - EVP Finance
applicable locations: All locations, all teams  
effective date: 2019-08-15


---

# Policy Text

All purchases made under the `ReqLogic` Item ID category `Consulting and Professional Services`, AND directly interact with:

1. Any internal systems managed by IT 
1. Any of our customer-facing products or
1. Are for services not rendered in the ordinary course of business

... requires additional documentation (an internal memo) that outlines the following:

- The business issue
- How the use of the vendor's service solves that issue
- If vendor is new, you must contact accounts payable and have them complete the Extended Vendor Setup Form
- Reason for selection of this vendor
- List of existing systems impacted
- Project and program management of the vendor relationship,
- Estimated timeline for SOW or vendor relationship (if ongoing)
- Completed [vendor checklist on security and privacy](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=8664&SearchId=0)
- Vendors vetting procedures and/or qualification of its consultant
- Payment terms - How much, how often (annual, monthly, quarterly) and terms (Net 30, Net 60)
- Contract start and end dates and
- Cancellation requirements

This memo will be reviewed and approved by the following prior to contract signing:

- CTO, EVP Finance and VP of IT Operations
- Security review by CISO
- Architecture Review Board for purchases that interact with customer-facing products
- EVP Legal

The preceding memo should be updated for relevant changes each time there is a new SOW, increase of cost of 10% over the initial requisition or other material changes to terms and conditions.

All purchases must be supported by a `ReQlogic` requisition before purchase even if payment method is a credit card. All purchases made before August 15, 2019 will not be subject to new purchase requirements, however renewal requirements would apply.
