title: Customer Suspension, Cancellation and Write-off
author: Dave Storer, Controller  
published: 2019-10-28  
policy level: Very Important  
approver(s): Buck Brody EVP Finance, Jim Hill EVP Trade Contractor, Howard Atkins EVP BPM, Mark Casaletto President Canada, Jon Kost EVP GC  
applicable locations: Sales, Customer Success and Customer Service – all locations
effective date: 2019-10-28
last updated: 2020-06-11 

---

# Policy text:

Subscriptions will be suspended/cancelled at the following number of days past invoice due date (or in the case of a renewal that does not have a signed contract, the renewal date):

- Subcontractor Record type:
	- Suspended: 15 days (except BidClerk walkup at 3 days)
	- Cancelled: 45 days for existing customers and 15 days past subscription start date for net new

- GC Record type:
	- Suspended: 45 days
	- Cancelled: 90 days
	- This does not apply to fax or non-recurring items for GC

- Manufacturer Record type:
	- Suspended: 45 days
	- Cancelled: 90 days

- Takeoff subscription types:
	- Suspended: 45 days
	- Cancelled: 90 days

Canadian subscriptions follow their respective record types as listed above.

GC and Manufacturer record types require a documented review of non-closed renewal quotes greater than 30 days past due each month by the respective product line EVP’s.

Extensions to cancelation deadlines listed above require pre-approval from Controller and can be extended a maximum of 30 days beyond the requirements above.  Cancellation hold requests should be submitted via the [Cancellation Hold Request Form](https://forms.office.com/Pages/ResponsePage.aspx?id=NaAXdS_PBEKPsjsv72alF5WXlGMTGbVNoSI4tQNCwXBURFpWNEhNMVgxTEEwNDJLQTExSU5KRERVRS4u).

Extension from suspension deadlines require pre-approval documented in Salesforce in writing by the team Manager and will not exceed cancellation.

In limited circumstances, indefinite suspension / cancellation extensions can be granted only with written pre-approval no later than 10 days prior to cancellation, obtained by both the EVP Finance and CEO.  These requests are to be sent via email with the Accounting Operations Manager copied.

Sales Ops will assume responsibility via SF reports for expired and past due subscriptions to manually suspend access until automation and in-app messaging can be built by Back Office.

---
