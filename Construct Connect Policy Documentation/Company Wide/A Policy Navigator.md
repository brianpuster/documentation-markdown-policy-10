title: DELEGATION OF AUTHORITY - POLICY NAVIGATOR 
author: Dave Storer, Controller
published: 2020-01-13


Please see [Delegation of Authority Name Directory](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=7559) for authority levels by individual.For any questions, please contact <accountspayable@constructconnect.com>.  
Note that certain navigation blocks contain links to associated policies or reference files.


<div class="mermaid">
graph LR

A[Vendor Purchase] -->|small one off|T[Travel or Incidentals]
TP --> C[Use P Card or Reimburse] --> Concur
T --> TP[TravelPolicy]
A --> ROP[Lease or Charitable]
A --> FormerEmployee --> DOA
A --> CapEx --> DOA
ROP --> DOA[See ROPER DoA]
A --> S[Software or Consulting] --> SP[SoftwarePolicy]
S --> ConsultingPolicy --> R[ReqLogic]
SP -->R
DOA --> R
A --> AO[All Other] --> R
R --> REQ[Requistion]
REQ --> Approved
Approved --> Sign
Sign --> Buy

subgraph Type
ROP
S
AO
T
CapEx
FormerEmployee 
end

subgraph Governing Policy
ConsultingPolicy
SP
DOA
TP
end

click ConsultingPolicy "https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing"

click SP "https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing"

click DOA "https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Delegation%20of%20Authority"

click TP "https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Travel%20and%20Entertainment"

click R "https://reqlogic.buildone.co/reqlogic/Login.aspx"

click Concur "https://www.concursolutions.com/nui/signin/v2"

</div>


