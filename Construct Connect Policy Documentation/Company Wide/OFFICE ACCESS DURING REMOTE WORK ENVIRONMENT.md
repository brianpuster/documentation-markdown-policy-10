title: OFFICE ACCESS DURING REMOTE WORK ENVIRONMENT    
author: Julie Storm, Chief People Officer    
published: 2020-09-16    
policy level: Very Important  
data classification: Internal
approver(s): Julie Storm, Chief People Officer
applicable Locations: All
effective date: 2020-09-16

---

# Policy Text

As you know, the health and safety of all team members continues to be our priority.  This is why we are currently working remotely and will not be returning to offices until at least June of 2021.  However, as our state and local communities stabilize from the COVID-19 pandemic we are allowing limited access to team members in accordance with state, local, and CDC guidelines.    

Our overall actions regarding office access will be guided by the following principles:  

- The health and safety of our team members will be our primary concern in all return to office access related decisions  
- Physical distancing (i.e. staying at least 6 feet from other people, not gathering in groups, staying out of crowded places) and regular hygiene will continue to be emphasized  
- Continued enhanced cleaning protocols, including frequent cleaning of high traffic/contact areas   
- Responding accordingly to any information suggesting active COVID-19 cases 

# Limited Access Requirements 

In order to get approval to access the office, team members will need to meet one of the following criteria: 

1. Need to access the office for essential tasks to be performed, as designated by their manager. 
1. Need to retrieve essential items required to perform work.  
1. Need for assistance with computer and technical issues that cannot be resolved virtually. 

# Office Access Request Process  

- Team members fill out the Office Access Form on the Loop. 
- The form will be sent to the Chief People Officer. Based on the information provided by the requesting team member, the request will be approved or denied and the team member will be contacted directly.   
- If approved, the team member will be provided a time to arrive at the office and will be met by an office representative.   

# Prior to Arriving at the Office 

Team members who have been approved may only access the office if:

- In the past 14 days they have not:
    - been diagnosed with COVID-19
    - experienced any COVID-19-related symptoms
    - in close contact with another person with COVID-19 or with COVID-19-related symptoms
    - have travelled internationally to any location listed in any Level 3 Travel Health Notice.
    
- Self-screening has occurred for any of the following new or worsening signs or symptoms of possible COVID-19 including:  
    - cough  
    - shortness of breath or difficulty breathing  
    - chills  
    - repeated shaking with chills 
    - muscle pain 
    - headache 
    - sore throat 
    - loss of taste or smell 
    - diarrhea 
    - feeling feverish or a measured temperature greater than or equal to 100.4 degrees Fahrenheit     

# Health and Safety Protocols while in the office 

While these guidelines are intended to promote health and safety throughout our offices, there is no substitute for good judgement. We understand we will not be able to operate risk-free. When accessing the office, all team members are reminded to be respectful of their co-worker’s feelings and reactions to being in an office environment with others. Everyone should fully cooperate with the guidelines and be tolerant of individual practices, concerns and feelings 

- Hand sanitizer, disinfectants/wipes and tissues will be made available in conspicuous locations.  
- Anti-bacterial wipes/disinfectants will be provided for use at or near shared equipment, including printers and copiers 
- A mask must be worn in all common areas, including the break area/kitchen, restrooms, conference rooms, collaborative spaces, huddle rooms and other shared space.  
- All common areas, including the conference rooms, collaborative spaces, huddle rooms and other shared space will not be utilized unless appropriate physical distancing measures can be taken, and People and Culture must be notified first.  
- Visitors or contractors must be preapproved by People and Culture.  
- In-person meetings should be limited to the number of team members that can practice appropriate physical distancing in the meeting area.   
- Avoid using other team members’ phones, desks, offices, or other work tools and equipment, when possible. If necessary, clean and disinfect them before and after use. 
- Practice physical distancing by avoiding large gatherings and maintaining distance (approximately 6 feet or 2 meters) from others when possible. If such distancing is not feasible, other measures such as face covering, hand hygiene, cough etiquette, cleanliness, and sanitation should be rigorously practiced.  
- Avoid handshaking – we encourage the use of other noncontact methods of greeting someone.  

# Modifications 

We reserve the right to modify this Policy in accordance with the needs of the Company and pursuant to guidance from public health authorities. We will continue to monitor the situation, follow all recommendations by the public health authorities, and provide you with further updates as applicable.  

Should you have any questions, please reach out to your manager or People and Culture.  

By your signature below, you acknowledge you have read, understand, and will follow the safety protocols outlined in the Company’s Office Access Policy You also acknowledge that while the Company will take every reasonable measure to keep team members safe, we will not be able to operate risk-free, and by coming to the office you accept responsibility for adhering to these safety protocols and will exercise good judgement and you freely assume such risks, both known and unknown.   

** Signed electronically. For questions see People & Culture**

# PRIVACY NOTICE

To reduce the risk of spreading the COVID-19 virus in and through the workplace and protect our team members and guests, the Company is implementing procedures for measuring the temperature of all individuals coming into the Company’s facility and inquiring and observing whether any individual attempting to enter a Company facility has any symptoms of COVID-19 or related illness. Any individual, whether a team member or visitor, whose temperature is measured to indicate a fever, who reports having or is observed to have any such symptoms, or who has recently been in contact with symptomatic individuals will not be permitted to enter any Company facility. 

We are notifying you that we are collecting the following category of Personal Information: Medical and health information, specifically your body temperature and whether you have or display certain symptoms such as fatigue, cough, sneezing, aches and pains, runny or stuffy nose, sore throat, diarrhea, headaches, or shortness of breath, whether you have recently been in close contact with anyone who has exhibited any of these symptoms, whether you have recently been in contact with anyone who has tested positive for COVID-19, and whether you have recently traveled to a restricted area that is under a Level 2, 3, or 4 Travel Advisory according to the U.S. State Department (including China, Italy, Iran, and most of Europe).  

The Company will maintain this information under conditions of confidentiality. We are collecting this information for purposes of reducing the risk of spreading the COVID-19 virus in and through the workplace and protecting our team members and guests.  

By signing below, I acknowledge and confirm that I have received and read and understand this disclosure.

** Signed electronically. For questions see People & Culture**