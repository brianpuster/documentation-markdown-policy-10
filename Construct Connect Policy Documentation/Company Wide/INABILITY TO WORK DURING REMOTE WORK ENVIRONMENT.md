title: INABILITY TO WORK DURING REMOTE WORK ENVIRONMENT    
author: Julie Storm, Chief People Officer    
published: 2020-10-27     
policy level: Important    
data classification: Internal  
approver(s):  Julie Storm, Chief People Officer; Jeff Cryder, EVP  
applicable Locations: All  
effective date: 2020-11-01  

---

# Policy text

If a team member is unable to work from home due to unforeseen instances, such as power outages or lack of working equipment the following guidelines should be followed:

## 8-Hour Workday or Less: 

If instances such as this occur, the team member should contact their manager immediately. If the equipment issue can be controlled by IT, they should be notified of the issue through SysAid immediately. If the issue cannot be addressed by IT, such as a power outage, you, as a manager, can make the determination whether time missed can be made up or if PTO should be taken.

## Greater Than an 8-Hour Workday: 

Issues that require a greater length of time to fix (longer than 1 (one), 8-hour workday), such as insufficient network connections or lack of phone availability or function, are the responsibility of the team member to rectify within a 24-hour period. This can mean having to find a different place to work if needed. Team members should contact their manager to determine a solution. In addition, PTO can be taken if work cannot be done and MUST be taken if a solution cannot be found within a 24-hour period. If the issue is with company provided equipment, your manager will work with IT to make the determination of next steps.
