title: Return from Furlough  
author: Julie Storm - Chief People Officer  
published: 2020-04-27  
policy level: Very Important  
approver(s): Julie Storm - Chief People Officer, Buck Brody EVP Finance 
applicable locations: All locations, all teams 
effective date: 2020-04-27 

---

# Policy Text 

Furloughed team members may be returned to work as business needs require. Business need will be determined by the ELT leader of the business unit in partnership with the EVP, Finance and CEO prior to an individual being returned to work.

Business need can be defined as (but not limited to) attrition, need for increased staff due to workload, need for increased customer support, or the returning of key positions to support the overall business, as examples.

Team members can also be brought back into a role that is different from their current role, should it benefit the business.

# Return Criteria

The return to work process requires that individuals are called back based on the priority ranking that was put in place by each team at the time of the furlough. Priority should be based on:

1. Business Need for Position
1. Performance level (highest performers at time of furlough return first)

Should a position need to be returned due to business need or filled due to attrition and there is no distinction for performance, seniority can be utilized as the deciding factor. Example: Two furloughed team members hold the same role and have the same level of performance, but one has 2 years of

service and the other has 1 year of service. The person with 2 years of service would be returned. This should be a rare occurrence.

Any exceptions to the established priority return process must be approved by the Chief People Officer in advance of the return.

# Return Process

Once a business need is identified by the ELT leader for the return of a furloughed team member, the ELT Leader should partner with the CEO to discuss the business need and return date. The EVP, Finance will then model the impact to the business and partner with the CEO to review the business case. The final decision will be made by the CEO.

Once the request has been reviewed by the CEO, the ELT Leader (or requesting hiring manager), Chief People Officer, and Sr. Director, FP&A will be notified of the decision via an approval e-mail from the CEO.

If a team member is approved to return, People and Culture will notify the direct manager to coordinate a call with the returning team member. The call will then take place between the returning team member, P&C and the direct manager to discuss the team member’s return to their previous position or a different position and the terms of the return from furlough letter, including the return date, compensation and benefits associated with the role. Should the salary reduction policy still be in place when the team member returns, they will receive the rate of pay that is in effect for the role’s current base pay at that time. If the salary reduction is no longer in force, the team member will be returned to their base salary prior being furloughed. A return date will also be agreed upon.

Once a date is agreed upon, People and Culture will notify IT (via a SysAid ticket ) to set up systems access and prepare the equipment. The hiring manager will need to supply the information needed for systems access for the ticket to be processed.

The return from furlough letter will be sent to the team member via DocuSign by People and Culture following the meeting and should be signed and returned within 24 hours.

If the team member declines to return to their original position, it will be viewed as a resignation and a notice of resignation will be required. If the team member is offered a different position that is at a lower compensation level, they will have the option of accepting or declining the role.

The next team member on the team’s priority list would then be approved and contacted.

