title: Company Name Change  
author: Beth Patrick, Accounting Operations Manager  
published: 2020-01-10  
policy level: Important Not Urgent  
approver(s): Dave Storer, Controller
applicable locations: All locations
effective date: 2020-01-10  


---

# Policy text:

All company name changes on existing billing accounts require documentation to support the change. If a full name change, it requires one of the following: Certificate of Registration, Certificate of Incorporation, or Acquisition documents. Most government documents will be accepted, minus a W-9.
If the change is minor, such as adding an LLC or a slight word change, then a W-9 will suffice.
In the event the required documentation is not available please contact accounting for guidance on

# Policy process:

An Accounting case type should still be used to update company info. The documents should be added to the google docs within Salesforce to support.

---

