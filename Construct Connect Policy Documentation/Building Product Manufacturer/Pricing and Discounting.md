title: Pricing and Discounting  
author: Eric Bodge - Assistant Controller   
published: 2020-08-31   
effective date: 2020-08-01   
policy level: Very Important   
Approver(s): Howard Atkins – EVP & GM Building Product Manufacturer, Buck Brody - EVP & GM Finance  
Location or Team Applicability: United States based core Building Product Manufacturer new sales, all locations   

---

# Policy Text:

- Quotes must be physically approved in the system by a person who has the authority level to approve it. Comments stating “approval was received” is not sufficient. Closed Opportunities will be audited for compliance and exceptions will be reported.
- As of 08/2020 pricing is calculated based off of "Pricing Calculator 26.2". Any changes to this pricing calculator require a Jira ticket with Controllership approval per Delegation of Authority 2.11
 
## Pricing and Discounting approval authority levels:
 
- Sales Rep - </= 10% of pricing calculator Final Annualized Grand Total
- Sales Manager/Director - </= 20% of pricing calculator Final Annualized Grand Total
- VP of Sales - >20% of pricing calculator Final Annualized Grand Total

## Violations to the pricing and discounting policy will be handled as follows:
 
- 1st offense – No commission on the deal and a verbal warning (at EVP's discretion) 
- 2nd offense – No commission on the deal and a written warning
- 3rd offense – No commission on the deal and a final written warning
- 4th offense – No commission on the deal and termination

---