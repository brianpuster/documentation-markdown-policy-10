title: Customer Success PIP
published: 2019-09-11  
author: Jim Hill - EVP and GM Trade Contractor  
effective date: 2020-04-01  
last updated: 2020-10-01  


# Metrics & Performance Improvement Plan Process

Customer Success Managers will be reviewed by C4Life Leadership monthly at the beginning of each month for the previous month’s performance against the individual’s established minimum monthly metrics. If a Customer Success Manager does not meet their full metrics, a performance improvement plan process will be implemented as outlined below. Customer Success Managers who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, under the Orientation Period Performance Guidelines which are documented below.  

# Orientation Period Performance Guidelines  

- Satisfactory attendance -- Per the Employee Handbook
    - If you are going to be late or out of any reason, you must contact your Manager in the form of a phone call or text prior to the start of your shift  
- Team engagement - defined by management  
    - Handbook + manager expectations
- Certification of the platform
    - Platform associated to the assigned customer-base
    - Team Member eligible for 2 attempts to be certified
- Attitude that is open to coaching and feedback  

*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during the orientation period, their employment could be terminated.*  

*Upon successful completion of the orientation period team members will enter the ongoing employment classification below*    

# Performance Metrics

The performance metrics for the Customer Success Manager will be focused on one (1) primary category based on your customer segment assignment:  

## First Year - Renewal & Default Performance  
- Standard  
    - 99.50% to monthly Renewal Performance goal with a rolling 12-month period  
    - 99.50% to the monthly Default Performance goal with a rolling 12-month period  
- Explanation  
    - Based on market and/or account assignment & the corresponding historical trends, each Customer Success Manager has an assigned percentage performance metric, which is defines the 100% to goal.  

## Legacy - Renewal & Default Performance  
- Standard   
    - 99.75% to monthly Renewal Performance goal with a rolling 12-month period  
    - 99.75% to the monthly Default Performance goal with a rolling 12-month period   
- Explanation  
    - Based on market and/or account assignment & the corresponding historical trends, each Customer Success Manager has an assigned percentage performance metric, which is defines the 100% to goal.  

These metrics are designed and upheld by management to ensure the best possible customer experience for all subscribers. A Performance Improvement Plan (PIP) will be issued to any team member in the event of failure to comply to the below performance metrics for three (3) months with in a 12-month period. The progression timeline of the Performance Improvement Plan is notated below.    

# Performance Improvement Process Post Orientation Period

- 1st Stage Performance Improvement Plan
    - below performance metrics for any three (3) months
- 2nd Stage Performance Improvement Plan
    - below performance metrics for any three (3) months
- 3rd Stage - Termination
    - below performance metrics for any three (3) months

A team member will exit the Performance Improvement Plan if they meet the standard in each metric outlined below. If the team member does not meet the standards in each metric outlined below, the team member will progress to the next stage Performance Improvement Plan. The same level Performance Improvement Plan will not be repeated in the same consecutive six (6) month period.  

# Additional Metrics

The following additional metrics will be the components by which management will assess during an assigned timeframe following the delivery of each Performance Improvement Plan (PIP).  

- Total Talk Time
    - Standard - 2 hours/day equating to 10 hours/week
    - Explanation - reporting sourced from Calabrio call report
- Average Talk Time
    - Standard - 3:30/call
    - Explanation - reporting sourced from Calabrio call report
- Dials  
    - Standard - 30 dials/day equating to 180 dials/week
    - Explanation - reporting sourced from Calabrio call report, excluding internal dials
- Salesforce and/or Gainsight Auto-Assigned Tasks  
    - Standard 
        - Legacy Customer Success Manager - 90% of tasks completed/week
        - First Customer Success Manager - No task can be more than 2 weeks past due    
    - Explanation - excluding team member created tasks  

# Policy Changes  

Management reserves the right to amend, replace, or discontinue this metric plan at its sole discretion. In the event of such a change, management will provide a minimum of thirty (30) days’ notice to the affected participants prior to such a change taking effect.  

I have read and acknowledge the above documentation to be the process I will be held accountable to regarding my performance as a Customer Success Manager.  

Management reserves the right to by-pass stages of the improvement process depending on the nature of the issue.  

Minimum monthly threshold as it pertains to the performance plan will be pro-rated for approved FMLA or bereavement absences.  

# Acknowledgement  

I have read, and acknowledge, the Customer Success Performance Improvement Plan Process to be the guideline to which I will be held accountable regarding my performance.    

